## Local development

```bash
k3d cluster create -p "80:80@loadbalancer" aurora
kubectl apply -f ./minio.yaml
kubectl apply -f ./crd.yaml
```

Minio server accessable on http://minio-controller.127.0.0.1.nip.io username:
`admin` password: `password`. And the minio endpoint url is http://minio.127.0.0.1.nip.io

Contoller can be run locally with

```bash
export AURORA_BUCKET=repo
export AWS_SECRET_ACCESS_KEY=password
export AWS_ACCESS_KEY_ID=admin
export AWS_ENDPOINT_URL=http://minio.127.0.0.1.nip.io
cargo run
```
