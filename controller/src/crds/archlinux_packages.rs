use kube::CustomResource;
use schemars::JsonSchema;
use serde::{Deserialize, Serialize};

#[derive(CustomResource, Debug, Clone, Deserialize, Serialize, JsonSchema)]
#[kube(group = "aurora.io", version = "v1", kind = "ArchLinuxPackage")]
#[kube(shortname = "pkg", namespaced)]
pub struct ArchLinuxPackageSpec {
    version: String,
}
