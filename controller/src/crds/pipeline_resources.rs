use k8s_openapi::apimachinery::pkg::apis::meta::v1::ObjectMeta;
use kube::{
    api::{Patch, PatchParams, Resource},
    Api, CustomResource,
};
use schemars::JsonSchema;
use serde::{Deserialize, Serialize};

use crate::Error;

use super::{namespace_from_object_meta, object_meta_from_generator_meta};

#[derive(CustomResource, Debug, Clone, Deserialize, Serialize, JsonSchema)]
#[kube(group = "tekton.dev", version = "v1alpha1", kind = "PipelineResource")]
#[kube(namespaced)]
pub struct PipelineResourceSpec {
    #[serde(rename = "type")]
    pub ty: String,
    pub params: Vec<PipelineResourceSpecParams>,
}

#[derive(Debug, Clone, Deserialize, Serialize, JsonSchema)]
pub struct PipelineResourceSpecParams {
    pub name: String,
    pub value: String,
}

pub async fn create_pipeline_resource(
    client: kube::Client,
    metadata: &ObjectMeta,
) -> Result<(), Error> {
    let namespace = namespace_from_object_meta(metadata)?;

    let pipeline_resource = PipelineResource {
        api_version: "tekton.dev/v1alpha1".to_string(),
        kind: PipelineResource::kind(&()).to_string(),
        metadata: object_meta_from_generator_meta(metadata)?,
        spec: PipelineResourceSpec {
            ty: "git".to_string(),
            params: vec![
                PipelineResourceSpecParams::new("url", "https://aur.archlinux.org/argo-bin.git"),
                PipelineResourceSpecParams::new("revision", "master"),
            ],
        },
    };

    Api::<PipelineResource>::namespaced(client.clone(), namespace)
        .patch(
            pipeline_resource
                .metadata
                .name
                .as_ref()
                .ok_or(Error::missing_object_key(".metadata.name"))?,
            &PatchParams::apply("archlinuxpackage.aurora.io"),
            &Patch::Apply(&pipeline_resource),
        )
        .await
        .map_err(|err| Error::JobCreationFailed { source: err })?;

    Ok(())
}

impl PipelineResourceSpecParams {
    pub fn new(name: impl Into<String>, value: impl Into<String>) -> Self {
        Self {
            name: name.into(),
            value: value.into(),
        }
    }
}
