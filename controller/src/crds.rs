use k8s_openapi::apimachinery::pkg::apis::meta::v1::{ObjectMeta, OwnerReference};
use kube::api::Resource;

use crate::Error;
pub mod archlinux_packages;
pub mod pipeline_resources;
pub mod pipeline_run;

use archlinux_packages::ArchLinuxPackage;

fn namespace_from_object_meta(metadata: &ObjectMeta) -> Result<&str, Error> {
    metadata
        .namespace
        .as_deref()
        .ok_or(Error::missing_object_key(".metadata.namespace"))
}

fn object_meta_from_generator_meta(metadata: &ObjectMeta) -> Result<ObjectMeta, Error> {
    let name = metadata
        .name
        .as_ref()
        .ok_or(Error::missing_object_key(".metadata.name"))?;
    let uid = metadata
        .uid
        .as_ref()
        .ok_or(Error::missing_object_key(".metadata.uid"))?;

    Ok(ObjectMeta {
        name: Some(name.clone()),
        owner_references: Some(vec![OwnerReference {
            controller: Some(true),
            api_version: ArchLinuxPackage::api_version(&()).to_string(),
            kind: ArchLinuxPackage::kind(&()).to_string(),
            name: name.to_string(),
            uid: uid.to_string(),
            ..OwnerReference::default()
        }]),
        ..Default::default()
    })
}
