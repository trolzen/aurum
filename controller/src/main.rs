use color_eyre::{eyre::WrapErr, Report, Result};
use futures::StreamExt;
use kube::{api::ListParams, Api};
use kube_runtime::controller::{Context, Controller, ReconcilerAction};
use thiserror::Error;
use tokio::time::Duration;

mod crds;

use crds::archlinux_packages::*;
use crds::pipeline_resources::*;
use crds::pipeline_run::*;

#[derive(Error, Debug)]
pub enum Error {
    #[error("failed to create job: {}", source)]
    JobCreationFailed {
        #[source]
        source: kube::Error,
    },
    #[error("missing object key: {}", name)]
    MissingObjectKey { name: &'static str },
}

// Data we want access to in error/reconcile calls
struct Data {
    kube_client: kube::Client,
    s3_client: s3::Client,
}

#[tokio::main]
async fn main() -> Result<()> {
    if std::env::var_os("RUST_LOG").is_none() {
        std::env::set_var("RUST_LOG", "info");
    }
    env_logger::init();
    color_eyre::install()?;

    let kube_client = kube::Client::try_default()
        .await
        .wrap_err_with(|| "failed to create kube client")?;
    let s3_client = bucket::create_s3_client(None, None, None)
        .wrap_err_with(|| "failed to create s3 client")?;
    bucket::create_bucket(&s3_client).await?;

    let alpkgs = Api::<ArchLinuxPackage>::all(kube_client.clone());
    let pipeline_resource_api = Api::<PipelineResource>::all(kube_client.clone());

    log::info!("starting archlinuxpackage-controller");

    Controller::new(alpkgs, ListParams::default())
        .owns(pipeline_resource_api, ListParams::default())
        .shutdown_on_signal()
        .run(
            reconcile,
            error_policy,
            Context::new(Data {
                kube_client,
                s3_client,
            }),
        )
        .for_each(|res| async move {
            match res {
                Ok(o) => log::info!("reconciled {:?}", o),
                Err(e) => log::warn!("reconcile failed: {}", Report::from(e)),
            }
        })
        .await;

    log::info!("controller terminated");
    Ok(())
}

/// Controller triggers this whenever our main object or our children changed
async fn reconcile(
    generator: ArchLinuxPackage,
    ctx: Context<Data>,
) -> Result<ReconcilerAction, Error> {
    log::info!("reconcile triggered");
    let client = ctx.get_ref().kube_client.clone();

    create_pipeline_resource(client.clone(), &generator.metadata).await?;
    create_pipeline_run(client.clone(), &generator.metadata).await?;

    Ok(ReconcilerAction {
        requeue_after: Some(Duration::from_secs(30)),
    })
}

/// The controller triggers this on reconcile errors
fn error_policy(_error: &Error, _ctx: Context<Data>) -> ReconcilerAction {
    ReconcilerAction {
        requeue_after: Some(Duration::from_secs(1)),
    }
}

impl Error {
    fn missing_object_key(name: &'static str) -> Error {
        Error::MissingObjectKey { name }
    }
}

mod bucket {
    use std::str::FromStr;

    use aws_types::region::ProvideRegion;
    use color_eyre::{
        eyre::{eyre, WrapErr},
        Result, Section,
    };

    pub fn create_s3_client(
        endpoint_url: Option<&http::Uri>,
        access_key_id: Option<&str>,
        secret_access_key: Option<&str>,
    ) -> Result<s3::Client> {
        let region = aws_types::region::default_provider()
            .region()
            .unwrap_or_else(|| s3::Region::new("us-west-2"));

        let config = s3::Config::builder().region(region);

        let config = match endpoint_url {
            Some(endpoint_url) => {
                config.endpoint_resolver(s3::Endpoint::immutable(endpoint_url.clone()))
            }
            None => match std::env::var("AWS_ENDPOINT_URL").ok().as_ref() {
                Some(endpoint_url) => {
                    let endpoint_url =
                        http::Uri::from_str(endpoint_url).wrap_err("invalid endpoint url")?;
                    if endpoint_url.scheme().is_none() {
                        Err(eyre!("missing scheme in endpoint url: {}", endpoint_url))
                            .with_note(|| format!(
                                "You need to specify the scheme in the endpoint url. Try one of the following:\n - http://{}\n - https://{}",
                                &endpoint_url,
                                &endpoint_url
                            ))?;
                    }
                    config.endpoint_resolver(s3::Endpoint::immutable(endpoint_url))
                }
                None => config,
            },
        };

        let config = match (access_key_id, secret_access_key) {
            (Some(access_key_id), Some(secret_access_key)) => config.credentials_provider(
                s3::Credentials::from_keys(access_key_id, secret_access_key, None),
            ),
            _ => config,
        };

        Ok(s3::Client::from_conf(config.build()))
    }

    pub async fn create_bucket(client: &s3::Client) -> Result<()> {
        let bucket = std::env::var("AURORA_BUCKET").wrap_err("failed to get env AURORA_BUCKET")?;
        match client.create_bucket().bucket(&bucket).send().await {
            Ok(_) => Ok(()),
            Err(s3::SdkError::ServiceError {
                err:
                    s3::error::CreateBucketError {
                        kind: s3::error::CreateBucketErrorKind::BucketAlreadyOwnedByYou(..),
                        ..
                    },
                ..
            }) => {
                log::debug!("bucket already exists: {}", &bucket);
                Ok(())
            }
            Err(err) => Err(err.into()),
        }
    }
}
