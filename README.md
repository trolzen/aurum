# Aurora

> A cloud based Arch Linux build system

## Getting started

### Requirements

- [k3d](https://k3d.io)
- [devspace](https://devspace.sh/cli/docs/getting-started/installation)

Bootstrap a cluster and start devspace dev mode:

```bash
./bootstrap-k3d.sh
devspace dev
```

Then inside the devspace container you can start the controller with:

```bash
cargo run
```
