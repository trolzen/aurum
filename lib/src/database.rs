use log::*;
use std::convert::TryInto;
use std::fs::create_dir;
use std::io::BufReader;
use zstd::Decoder;

use crate::package::Package;
use bzip2::{self, read::BzDecoder, write::BzEncoder};
use flate2::{self, read::GzDecoder, write::GzEncoder};
use glob::glob;
use sha2::{Digest, Sha256};
use std::io::{Error as IoError, ErrorKind as IoErrorKind};
use std::{
    ffi::{OsStr, OsString},
    fs::{read_to_string, ReadDir},
    str::FromStr,
};
use std::{fs, io::Read};
use std::{fs::read_dir, io::Result};
use std::{
    fs::File,
    io::Write,
    path::{Path, PathBuf},
};
use tar::{Archive, Builder};
use thiserror::Error;
use xz2::{read::XzDecoder, write::XzEncoder};
use zstd::{Decoder as ZstdDecoder, Encoder as ZstdEncoder};

const CHUNK_SIZE: usize = 8 * 1024 * 1024;

mod desc_file;
use desc_file::DbDesc;

use self::desc_file::FilesDesc;

#[derive(Debug)]
pub struct Database {
    location: tempfile::TempDir,
}

#[derive(Debug, Default, Eq, PartialEq, Clone, Hash)]
struct PackageKey(PathBuf);

impl Database {
    pub fn new() -> Result<Database> {
        Ok(Database {
            location: tempfile::Builder::new().prefix("aurora-").tempdir()?,
        })
    }
}

#[derive(Debug, Default, PartialEq, Clone)]
pub struct PackageDesc {
    pub name: String,
    pub version: String,
    pub filename: Option<String>,
    pub md5sum: Option<String>,
    pub sha256sum: Option<String>,
    pub base: Option<String>,
    pub desc: Option<String>,
    pub url: Option<String>,
    pub arch: Option<String>,
    pub builddate: Option<String>,
    pub packager: Option<String>,
    pub isize: Option<i64>,
    pub csize: Option<i64>,
    pub groups: Vec<String>,
    pub licenses: Vec<String>,
    pub conflicts: Vec<String>,
    pub replaces: Vec<String>,
    pub provides: Vec<String>,
    pub depends: Vec<String>,
    pub optdepends: Vec<String>,
    pub makedepends: Vec<String>,
    pub checkdepends: Vec<String>,
    pub files: Option<Vec<String>>,
}

impl PackageDesc {
    fn from(
        name: impl Into<String>,
        version: impl Into<String>,
        db_desc: DbDesc,
        files_desc: Option<FilesDesc>,
    ) -> PackageDesc {
        let name = name.into();
        let version = version.into();
        if db_desc.name.is_some() && Some(&name) != db_desc.name.as_ref() {
            warn!(
                "database is inconsistent: name mismatch on package {}",
                &name
            );
        }
        if db_desc.version.is_some() && Some(&version) != db_desc.version.as_ref() {
            warn!(
                "database is inconsistent: version mismatch on package {}",
                &version
            );
        }
        PackageDesc {
            name,
            version,
            filename: db_desc.filename,
            md5sum: db_desc.md5sum,
            sha256sum: db_desc.sha256sum,
            base: db_desc.base,
            desc: db_desc.desc,
            url: db_desc.url,
            arch: db_desc.arch,
            builddate: db_desc.builddate,
            packager: db_desc.packager,
            isize: db_desc.isize,
            csize: db_desc.csize,
            groups: db_desc.groups,
            licenses: db_desc.licenses,
            conflicts: db_desc.conflicts,
            replaces: db_desc.replaces,
            provides: db_desc.provides,
            depends: db_desc.depends,
            optdepends: db_desc.optdepends,
            makedepends: db_desc.makedepends,
            checkdepends: db_desc.checkdepends,
            files: files_desc.map(|desc| desc.files),
        }
    }
}

impl Database {
    fn db_path(&self) -> Result<PathBuf> {
        let path = self.location.path().join("db");
        match create_dir(&path) {
            Err(err) if err.kind() == std::io::ErrorKind::AlreadyExists => {}
            v => v?,
        }
        Ok(path)
    }

    pub fn from_reader(reader: impl Read) -> Result<Self> {
        let location = tempfile::Builder::new().prefix("aurora-").tempdir()?;
        let db_path = location.path().join("db");

        fs::create_dir(&db_path)?;
        Archive::new(reader).unpack(&db_path)?;

        Ok(Database { location })
    }

    pub fn from_file(db_file: impl AsRef<Path>) -> Result<Self> {
        let db_file = db_file.as_ref();
        let database_file = File::open(db_file)?;
        let database_file: Box<dyn Read> = match (
            db_file.with_extension("")
                .with_extension("")
                .extension()
                .map(to_string)
                .as_ref()
                .map(|s| s.as_str()),
            db_file.with_extension("")
                .extension()
                .map(to_string)
                .as_ref()
                .map(|s| s.as_str()),
            db_file.extension()
                .map(to_string)
                .as_ref()
                .map(|s| s.as_str()),
        ) {
            (_, Some("db" | "files"), Some("tar")) => Box::new(database_file),
            (Some("db" | "files"), Some("tar"), Some("zst")) => Box::new(ZstdDecoder::new(database_file)?),
            (Some("db" | "files"), Some("tar"), Some("xz")) => Box::new(XzDecoder::new(database_file)),
            (Some("db" | "files"), Some("tar"), Some("gz")) => Box::new(GzDecoder::new(database_file)),
            (Some("db" | "files"), Some("tar"), Some("bz2")) => Box::new(BzDecoder::new(database_file)),
            (None, None, None) => return Err(new_error(
                format!("no extension on database file '{}', file needs to be a .db.tar or .db.tar.<COMPRESSION>", db_file.display()),
            )),
            (_, _, _) => return Err(new_error(format!("invalid extension on '{}', file needs to be a .db.tar or .db.tar.<COMPRESSION>", db_file.display()))),
        };

        Self::from_reader(database_file)
    }

    pub fn iter(&self) -> Result<DatabaseIter> {
        Ok(DatabaseIter {
            dir_iter: read_dir(self.db_path()?)?,
        })
    }

    pub fn try_is_empty(&self) -> Result<bool> {
        Ok(self.try_len()? == 0)
    }

    pub fn try_len(&self) -> Result<usize> {
        Ok(fs::read_dir(self.db_path()?)?.count())
    }

    pub fn write(&self, writer: impl Write) -> Result<()> {
        Builder::new(writer).append_dir_all("", self.db_path()?)
    }

    pub fn write_to_file(&self, db_file: impl AsRef<Path>) -> Result<()> {
        let db_file = db_file.as_ref();
        let database_file = File::create(db_file)?;
        let database_file: Box<dyn Write> = match (
            db_file.with_extension("")
                .with_extension("")
                .extension()
                .map(to_string)
                .as_ref()
                .map(|s| s.as_str()),
            db_file.with_extension("")
                .extension()
                .map(to_string)
                .as_ref()
                .map(|s| s.as_str()),
            db_file.extension()
                .map(to_string)
                .as_ref()
                .map(|s| s.as_str()),
        ) {
            (_, Some("db" | "files"), Some("tar")) => Box::new(database_file),
            (Some("db" | "files"), Some("tar"), Some("zst")) => Box::new(ZstdEncoder::new(database_file, 0)?),
            (Some("db" | "files"), Some("tar"), Some("xz")) => Box::new(XzEncoder::new(database_file, 6)),
            (Some("db" | "files"), Some("tar"), Some("gz")) => Box::new(GzEncoder::new(database_file, flate2::Compression::default())),
            (Some("db" | "files"), Some("tar"), Some("bz2")) => Box::new(BzEncoder::new(database_file, bzip2::Compression::default())),
            (None, None, None) => return Err(new_error(
                format!("no extension on database file '{}', file needs to be a .db.tar or .db.tar.<COMPRESSION>", db_file.display()),
            )),
            (_, _, _) => return Err(new_error(format!("invalid extension on '{}', file needs to be a .db.tar or .db.tar.<COMPRESSION>", db_file.display()))),
        };

        self.write(database_file)
    }

    pub fn remove_package(&mut self, name: &str, version: &str) -> Result<()> {
        fs::remove_dir_all(self.db_path()?.join(format!("{}-{}", name, version)))
    }

    pub fn remove_package_all(&mut self, name: impl AsRef<str>) -> color_eyre::Result<()> {
        // TODO check to see if package does not exist and return error if not
        for entry in glob(
            self.db_path()?
                .join(name.as_ref().to_string() + "*")
                .to_string_lossy()
                .as_ref(),
        )? {
            let entry = entry?;
            debug!("Removing directory '{}'", entry.display());
            fs::remove_dir_all(entry)?;
        }

        Ok(())
    }

    pub fn add_package(&mut self, package_path: impl AsRef<Path>, add_files: bool) -> Result<()> {
        let package_path = package_path.as_ref();

        let (md5sum, sha256sum) = {
            let mut file = File::open(package_path)?;
            let mut md5_hasher = md5::Context::new();
            let mut sha256_hasher = Sha256::new();
            let mut buf = vec![0u8; CHUNK_SIZE].into_boxed_slice();

            loop {
                let length = file.read(&mut buf)?;
                if length == 0 {
                    break;
                }
                md5_hasher.consume(&buf[0..length]);
                sha256_hasher.update(&buf[0..length]);
            }

            (
                format!("{:x}", md5_hasher.compute()),
                format!("{:x}", sha256_hasher.finalize()),
            )
        };

        let pkg_info = Package::from_path(package_path)?.pkg_info().unwrap(); // TODO

        let mut desc: desc_file::DbDesc = pkg_info.into();
        desc.filename = Some(
            package_path
                .file_name()
                .unwrap()
                .to_str()
                .unwrap()
                .to_string(),
        );
        desc.md5sum = Some(md5sum.to_string());
        desc.sha256sum = Some(sha256sum.to_string());
        desc.csize = Some(package_path.metadata()?.len() as i64);

        let db_package = self.db_path()?.join(format!(
            "{}-{}",
            desc.name.as_ref().unwrap(),
            desc.version.as_ref().unwrap()
        ));
        fs::create_dir_all(&db_package)?;
        fs::write(db_package.join("desc"), &desc.to_string())?;

        if add_files {
            let files: desc_file::FilesDesc = {
                let pkg_file = File::open(&package_path)?;
                let reader: Box<dyn Read> = match package_path.extension() {
                    Some(ext) => match ext.to_str() {
                        Some("zst") => Box::new(Decoder::new(pkg_file)?),
                        Some("tar") => Box::new(pkg_file),
                        Some("xz") => Box::new(XzDecoder::new(pkg_file)),
                        Some("gz") => {
                            let buffer = BufReader::new(pkg_file);
                            Box::new(GzDecoder::new(buffer))
                        }
                        Some("bz2") => Box::new(BzDecoder::new(pkg_file)),
                        _ => unimplemented!(),
                    },
                    _ => unimplemented!(),
                };
                Archive::new(reader).try_into()?
            };
            fs::write(db_package.join("files"), &files.to_string())?;
        }

        Ok(())
    }
}

pub struct DatabaseIter {
    dir_iter: ReadDir,
}

impl Iterator for DatabaseIter {
    type Item = std::result::Result<PackageDesc, DatabaseError>;

    fn next(&mut self) -> Option<Self::Item> {
        fn read_desc_file(path: impl AsRef<Path>) -> std::io::Result<DbDesc> {
            let desc = read_to_string(path.as_ref())?;
            Ok(desc_file::DbDesc::from_str(&desc).unwrap())
        }

        fn read_files_file(path: impl AsRef<Path>) -> std::io::Result<Option<FilesDesc>> {
            match read_to_string(path.as_ref()) {
                Ok(desc) => Ok(Some(desc_file::FilesDesc::from_str(&desc).unwrap())),
                Err(err) if err.kind() == std::io::ErrorKind::NotFound => Ok(None),
                Err(err) => return Err(err.into()),
            }
        }

        match self.dir_iter.next() {
            None => None,
            Some(Err(e)) => Some(Err(e.into())),
            Some(Ok(entry)) => {
                let (name, version) = match path_to_name_and_version(&entry.path()) {
                    Ok(v) => v,
                    Err(err) => return Some(Err(err)),
                };
                let db_desc = match read_desc_file(entry.path().join("desc")) {
                    Ok(v) => v,
                    Err(err) => return Some(Err(err.into())),
                };
                let files_desc = match read_files_file(entry.path().join("files")) {
                    Ok(v) => v,
                    Err(err) => return Some(Err(err.into())),
                };
                Some(Ok(PackageDesc::from(name, version, db_desc, files_desc)))
            }
        }
    }
}

fn path_to_name_and_version(path: &Path) -> std::result::Result<(String, String), DatabaseError> {
    let file_name = path.file_name().unwrap();
    let file_name = file_name
        .to_str()
        .ok_or_else(|| DatabaseError::InvalidUnicode(file_name.to_owned()))?;
    let (version_index, _) = file_name
        .rmatch_indices("-")
        .skip(1)
        .next()
        .ok_or_else(|| DatabaseError::InvalidDBEntry(file_name.to_string()))?;
    let (name, version) = file_name.split_at(version_index);
    Ok((name.to_string(), version[1..].to_string()))
}

#[derive(Error, Debug)]
pub enum DatabaseError {
    #[error("io error")]
    Io(#[from] IoError),
    #[error("invalid database entry: {0}")]
    InvalidDBEntry(String),
    #[error("invalid unicode in entry name: {0:?}")]
    InvalidUnicode(OsString),
}

#[cfg(test)]
mod tests {
    use super::*;
    use proptest::prelude::*;

    proptest! {
        #[test]
        fn to_string_then_from_str_produces_the_same_desc(name in "[^/\\pC]+", version in "[^-/\\pC]+-[^-/\\pC]+") {
            let path = PathBuf::from(format!("prefix/{}-{}", name, version));
            let (parsed_name, parsed_version) = path_to_name_and_version(&path).unwrap();
            assert_eq!(parsed_name, name);
            assert_eq!(parsed_version, version);
        }
    }
}

fn to_string(s: &OsStr) -> String {
    s.to_string_lossy().into_owned()
}
fn new_error(msg: String) -> IoError {
    IoError::new(IoErrorKind::InvalidInput, msg)
}
