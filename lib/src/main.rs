use log::LevelFilter;
use log::*;
use std::io::Write;

use std::path::PathBuf;

use aurora::{database::PackageDesc, DbPath};
use clap::{AppSettings, Clap};
use color_eyre::eyre::{Result, WrapErr};

#[tokio::main]
async fn main() -> Result<()> {
    let opts = Opts::parse();

    init_env_logger(opts.verbose);
    color_eyre::install()?;

    match opts.action {
        Action::Add(add) => add.run().await,
        Action::Remove(remove) => remove.run().await,
        Action::List(list) => list.run().await,
    }
}

#[derive(Clap, Debug)]
#[clap(version = "0.0.1")]
#[clap(setting = AppSettings::ColoredHelp)]
struct Opts {
    #[clap(short, long, parse(from_occurrences))]
    verbose: u8,
    #[clap(subcommand)]
    action: Action,
}

#[derive(Clap, Debug)]
enum Action {
    Add(Add),
    Remove(Remove),
    List(List),
}

#[derive(Clap, Debug)]
#[clap(version = "0.0.1")]
#[clap(setting = AppSettings::ColoredHelp)]
struct Add {
    database: DbPath,
    packages: Vec<PathBuf>,
}

#[derive(Clap, Debug)]
#[clap(version = "0.0.1")]
#[clap(setting = AppSettings::ColoredHelp)]
struct Remove {
    database: DbPath,
    packages: Vec<String>,
}

#[derive(Clap, Debug)]
#[clap(version = "0.0.1")]
#[clap(setting = AppSettings::ColoredHelp)]
struct List {
    database: DbPath,
    pattern: Option<String>,
}

impl Add {
    pub async fn run(self) -> Result<()> {
        let mut database = self.database.fetch_db().await?;
        let mut files = self.database.fetch_files().await?;

        for package in &self.packages {
            info!("Adding package '{}'", package.display());
            database
                .add_package(&package, false)
                .wrap_err_with(|| format!("Failed to add package '{}'", package.display()))?;
            files
                .add_package(&package, true)
                .wrap_err_with(|| format!("Failed to add package '{}'", package.display()))?;
        }

        for package in &self.packages {
            self.database.write_package(package).await?;
        }

        self.database.write_db(&database).await?;
        self.database.write_files(&files).await?;
        Ok(())
    }
}

impl Remove {
    pub async fn run(self) -> Result<()> {
        let mut database = self.database.fetch_db().await?;
        let mut files = self.database.fetch_files().await?;

        let packages: Vec<PackageDesc> = database
            .iter()?
            .filter(|package| {
                package
                    .as_ref()
                    .map(|package| self.packages.contains(&package.name))
                    .unwrap_or(false)
            })
            .collect::<Result<_, _>>()?;

        for package in &packages {
            info!("Removing package '{}-{}'", package.name, package.version);
            database.remove_package(&package.name, &package.version)?;
            files.remove_package(&package.name, &package.version)?;
        }

        for package in &packages {
            self.database
                .remove_package(&package.filename.as_ref().unwrap())
                .await?;
        }

        self.database.write_db(&database).await?;
        self.database.write_files(&database).await?;
        Ok(())
    }
}

impl List {
    pub async fn run(self) -> Result<()> {
        let database = self.database.fetch_db().await?;
        let pattern_exists = &self.pattern.is_some();
        let pattern = self.pattern.unwrap_or_default();

        for package in database.iter()? {
            let package = package?;
            if (*pattern_exists && package.name.contains(&pattern)) || (!*pattern_exists) {
                println!(
                    "{} {}\n  {}",
                    package.name,
                    package.version,
                    package
                        .desc
                        .or(Some("no description available".to_string()))
                        .unwrap()
                );
            }
        }

        Ok(())
    }
}

fn init_env_logger(verbose: u8) {
    let mut builder = env_logger::Builder::from_default_env();
    builder
        .format(|buf, record| writeln!(buf, "{}: {}", record.level(), record.args()))
        .filter(
            None,
            match verbose {
                0 => LevelFilter::Warn,
                1 => LevelFilter::Info,
                2 => LevelFilter::Debug,
                _ => LevelFilter::Trace,
            },
        )
        .init();
}
