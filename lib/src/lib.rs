use std::{
    ffi::OsStr,
    fs,
    os::unix::{fs::symlink, prelude::OsStrExt},
    path::{Path, PathBuf},
    str::FromStr,
};

use aws_types::region::ProvideRegion;
use bytes::Buf;
use color_eyre::eyre::{self, eyre, WrapErr};
use http::Uri;
use log::*;

pub mod database;
pub mod error;
pub mod package;
#[cfg(test)]
mod tests;

use error::{AuroraError, Result};

pub use database::Database;
pub use package::Package;
use s3::Config;

#[derive(Debug)]
pub enum DbPath {
    S3 { uri: Uri, client: s3::Client },
    Local(PathBuf),
}

impl DbPath {
    pub async fn fetch_db(&self) -> eyre::Result<Database> {
        match self {
            DbPath::S3 { uri, client } => {
                Ok(Self::open_s3_database(&client, Self::bucket_from(uri)?, uri.path()).await?)
            }
            Self::Local(path) => Self::open_local_database(path),
        }
    }

    pub async fn fetch_files(&self) -> eyre::Result<Database> {
        match self {
            DbPath::S3 { uri, client } => Ok(Self::open_s3_database(
                &client,
                Self::bucket_from(uri)?,
                Self::db_to_files_path(uri.path()).to_str().unwrap(),
            )
            .await?),
            Self::Local(path) => Self::open_local_database(Self::db_to_files_path(&path)),
        }
    }

    pub async fn write_db(&self, database: &Database) -> eyre::Result<()> {
        match self {
            DbPath::S3 { uri, client } => {
                let mut db_file = Self::create_tmp_database_file_for(uri.path())?;
                Self::write_database(database, &mut db_file)?;
                debug!("Uploading database to '{}'", uri);
                Self::upload_s3(&client, Self::bucket_from(uri)?, uri.path(), db_file.path())
                    .await?;
            }
            DbPath::Local(path) => {
                debug!("Writing database '{}'", path.display());
                Self::write_database(database, &path)?;
                Self::create_symlink(&path)?;
            }
        }
        Ok(())
    }

    pub async fn write_files(&self, database: &Database) -> eyre::Result<()> {
        match self {
            DbPath::S3 { uri, client } => {
                let bucket = Self::bucket_from(uri)?;
                let path = Self::db_to_files_path(uri.path());
                let path = path.to_str().unwrap();
                let mut db_file = Self::create_tmp_database_file_for(path)?;
                Self::write_database(database, &mut db_file)?;
                debug!("Uploading database to 's3://{}{}'", bucket, path);
                Self::upload_s3(&client, bucket, path, db_file.path()).await?;
            }
            DbPath::Local(path) => {
                let path = Self::db_to_files_path(&path);
                debug!("Writing database '{}'", path.display());
                Self::write_database(database, &path)?;
                Self::create_symlink(&path)?;
            }
        }
        Ok(())
    }

    pub async fn write_package(&self, package: impl AsRef<Path>) -> eyre::Result<()> {
        match self {
            DbPath::S3 { uri, client } => {
                let package = package.as_ref();
                let package_name = package.file_name().unwrap();
                let bucket = Self::bucket_from(uri)?;
                let s3_prefix = Self::db_to_files_path(uri.path());
                let s3_prefix = s3_prefix.parent().unwrap();
                let path = s3_prefix.join(package_name);
                debug!("Uploading package to 's3://{}{}'", bucket, path.display());
                Self::upload_s3(&client, bucket, path.to_str().unwrap(), package).await?;
            }
            DbPath::Local(_path) => {}
        }
        Ok(())
    }

    pub async fn remove_package(&self, package: impl AsRef<str>) -> eyre::Result<()> {
        match self {
            DbPath::S3 { uri, client } => {
                let bucket = Self::bucket_from(uri)?;
                let s3_prefix = Self::db_to_files_path(uri.path());
                let s3_prefix = s3_prefix.parent().unwrap();
                let key = s3_prefix
                    .join(package.as_ref())
                    .to_str()
                    .unwrap()
                    .to_string();
                debug!("Removing package from 's3://{}{}'", bucket, key);
                client
                    .delete_object()
                    .bucket(bucket)
                    .key(key)
                    .send()
                    .await?;
            }
            DbPath::Local(_path) => {}
        }
        Ok(())
    }

    fn open_local_database(path: impl AsRef<Path>) -> eyre::Result<Database> {
        let path = path.as_ref();
        Ok(if !path.exists() {
            debug!("Database not found, creating new database");
            Database::new()?
        } else {
            debug!("Opening database '{}'", path.display());
            Database::from_file(&path)
                .wrap_err_with(|| format!("Failed to open database '{}'", path.display()))?
        })
    }

    async fn open_s3_database(
        client: &s3::Client,
        bucket: impl Into<String>,
        path: impl Into<String>,
    ) -> Result<Database> {
        match Self::fetch_s3(client, bucket, path).await {
            Ok(db_file) => Ok(Database::from_file(db_file.path())?),
            Err(err) if err.is_not_exists() => Ok(Database::new()?),
            Err(err) => Err(err),
        }
    }

    fn bucket_from(uri: &Uri) -> eyre::Result<&str> {
        uri.host().ok_or_else(|| eyre!("missing bucket in s3 uri"))
    }

    async fn fetch_s3(
        client: &s3::Client,
        bucket: impl Into<String>,
        key: impl Into<String>,
    ) -> Result<tempfile::NamedTempFile> {
        let key = key.into();
        let mut dest = Self::create_tmp_database_file_for(&key)?;

        let resp = client.get_object().bucket(bucket).key(key).send().await?;
        let data = resp.body.collect().await?;

        std::io::copy(&mut data.reader(), &mut dest)?;

        Ok(dest)
    }

    async fn upload_s3(
        client: &s3::Client,
        bucket: impl Into<String>,
        key: impl Into<String>,
        db_file: impl AsRef<Path>,
    ) -> eyre::Result<()> {
        client
            .put_object()
            .bucket(bucket)
            .key(key.into())
            .body(s3::ByteStream::from_path(db_file).await?)
            .send()
            .await?;

        Ok(())
    }

    fn create_tmp_database_file_for(db_path: impl AsRef<Path>) -> Result<tempfile::NamedTempFile> {
        let db_path = db_path.as_ref();
        let file_name = db_path
            .file_name()
            .ok_or_else(|| AuroraError::MissingFileFromS3Uri(db_path.to_path_buf()))
            .map(|f| f.to_str().unwrap())?;

        Ok(tempfile::Builder::new().suffix(file_name).tempfile()?)
    }

    fn db_to_files_path(db_file: impl AsRef<Path>) -> PathBuf {
        let db_file = db_file.as_ref();
        let (db_name, db_ext) = Self::split_db_file_name(db_file);
        let mut files_file = db_name.to_os_string();
        files_file.push(OsStr::new(".files."));
        files_file.push(db_ext);
        db_file.with_file_name(files_file)
    }

    fn split_db_file_name<'a>(db_file: &'a Path) -> (&'a OsStr, &'a OsStr) {
        let file_name = db_file.file_name().unwrap().as_bytes();
        let (index, _) = file_name
            .windows(4)
            .enumerate()
            .rev()
            .find(|(_, s)| s == b".db.")
            .unwrap();
        (
            OsStr::from_bytes(&file_name[..index]),
            OsStr::from_bytes(&file_name[index + 4..]),
        )
    }

    fn write_database(database: &Database, db_file: impl AsRef<Path>) -> eyre::Result<()> {
        let db_file = db_file.as_ref();
        let mut extension = db_file.extension().unwrap_or_default().to_os_string();
        extension.push(".old");
        let backup_file = db_file.with_extension(&extension);

        match fs::rename(db_file, &backup_file) {
            Err(err) if err.kind() == std::io::ErrorKind::NotFound => {}
            result => result.wrap_err_with(|| {
                format!(
                    "Failed to backup database '{}' to '{}'",
                    db_file.display(),
                    backup_file.display()
                )
            })?,
        }

        database
            .write_to_file(db_file)
            .wrap_err_with(|| format!("Failed to write database '{}'", db_file.display()))?;
        Ok(())
    }

    fn create_symlink(db_file: impl AsRef<Path>) -> eyre::Result<()> {
        let db_file = db_file.as_ref();
        let db_symlink = Self::to_sym_link_path(db_file).unwrap();
        match fs::remove_file(&db_symlink) {
            Err(err) if err.kind() == std::io::ErrorKind::NotFound => {}
            result => result.wrap_err_with(|| {
                format!("Failed to remove db symlink '{}'", db_symlink.display())
            })?,
        }
        symlink(db_file.file_name().unwrap(), &db_symlink).wrap_err_with(|| {
            format!(
                "Failed to write database symlink '{}'",
                db_symlink.display()
            )
        })?;
        Ok(())
    }

    fn to_sym_link_path(db_path: impl AsRef<Path>) -> Option<PathBuf> {
        let db_path = db_path.as_ref().as_os_str().as_bytes();
        let db_index = db_path
            .windows(4)
            .enumerate()
            .rev()
            .find(|(_, s)| s == b".db.")
            .map(|(i, _)| i);
        let files_index = db_path
            .windows(7)
            .enumerate()
            .rev()
            .find(|(_, s)| s == b".files.")
            .map(|(i, _)| i);
        match (db_index, files_index) {
            (None, None) => None,
            (Some(db_index), None) => Some(OsStr::from_bytes(&db_path[..db_index + 3]).into()),
            (None, Some(files_index)) => {
                Some(OsStr::from_bytes(&db_path[..files_index + 6]).into())
            }
            (Some(db_index), Some(files_index)) => {
                if db_index > files_index {
                    Some(OsStr::from_bytes(&db_path[..db_index + 3]).into())
                } else {
                    Some(OsStr::from_bytes(&db_path[..files_index + 6]).into())
                }
            }
        }
    }
}

impl FromStr for DbPath {
    type Err = AuroraError;

    fn from_str(s: &str) -> eyre::Result<Self, Self::Err> {
        if s.starts_with("s3://") {
            Ok(Self::S3 {
                uri: Uri::from_str(s).map_err(|err| AuroraError::InvalidS3Uri {
                    s3_uri: s.to_string(),
                    src: err,
                })?,
                client: create_s3_client(None, None, None)?,
            })
        } else {
            let path = PathBuf::from_str(s).unwrap();
            Ok(Self::Local(path))
        }
    }
}

fn create_s3_client(
    endpoint_url: Option<&Uri>,
    access_key_id: Option<&str>,
    secret_access_key: Option<&str>,
) -> Result<s3::Client> {
    let region = aws_types::region::default_provider()
        .region()
        .unwrap_or_else(|| s3::Region::new("us-west-2"));

    let config = Config::builder().region(region);

    let config = match endpoint_url {
        Some(endpoint_url) => {
            config.endpoint_resolver(s3::Endpoint::immutable(endpoint_url.clone()))
        }
        None => match std::env::var("AWS_ENDPOINT_URL").ok().as_ref() {
            Some(endpoint_url) => {
                let endpoint_url = Uri::from_str(endpoint_url).map_err(|err| {
                    AuroraError::InvalidAwsEndpointUrl {
                        endpoint_url: endpoint_url.to_string(),
                        src: err,
                    }
                })?;
                config.endpoint_resolver(s3::Endpoint::immutable(endpoint_url))
            }
            None => config,
        },
    };

    let config = match (access_key_id, secret_access_key) {
        (Some(access_key_id), Some(secret_access_key)) => config.credentials_provider(
            s3::Credentials::from_keys(access_key_id, secret_access_key, None),
        ),
        _ => config,
    };

    Ok(s3::Client::from_conf(config.build()))
}
