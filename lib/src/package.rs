use bzip2::read::BzDecoder;
use flate2::bufread::GzDecoder;
use log::*;
use std::fs::File;
use std::io::BufReader;
use std::io::Read;
use std::path::Path;
use tar::Archive;
use thiserror::Error;
use xz2::read::XzDecoder;
use zstd::Decoder;

pub struct Package<R: Read> {
    archive: Archive<R>,
}

impl Package<Box<dyn Read>> {
    pub fn from_path(path: impl AsRef<Path>) -> std::io::Result<Package<Box<dyn Read>>> {
        let pkg_file = File::open(&path)?;
        let reader: Box<dyn Read> = match path.as_ref().extension() {
            Some(ext) => match ext.to_str() {
                Some("zst") => Box::new(Decoder::new(pkg_file)?),
                Some("tar") => Box::new(pkg_file),
                Some("xz") => Box::new(XzDecoder::new(pkg_file)),
                Some("gz") => {
                    let buffer = BufReader::new(pkg_file);
                    Box::new(GzDecoder::new(buffer))
                }
                Some("bz2") => Box::new(BzDecoder::new(pkg_file)),
                _ => unimplemented!(),
            },
            _ => unimplemented!(),
        };
        Ok(Package {
            archive: Archive::new(reader),
        })
    }
}

impl<R: Read> Package<R> {
    pub fn from_archive(archive: Archive<R>) -> Self {
        Self { archive }
    }

    pub fn pkg_info(mut self) -> Result<PkgInfo, PkgError> {
        let pkg_info = self.archive.entries()?.find(|entry| match entry {
            Ok(entry) => match entry.header().path() {
                Ok(path) => path == Path::new(".PKGINFO"),
                Err(_) => true,
            },
            Err(_) => true,
        });
        match pkg_info {
            None => Err(PkgError::MissingPkgInfo),
            Some(Err(err)) => Err(PkgError::from(err)),
            Some(Ok(mut entry)) => {
                let mut s = String::new();
                entry.read_to_string(&mut s)?;
                PkgInfo::parse(&s)
            }
        }
    }
}

#[derive(Debug, Clone, PartialEq)]
pub struct PkgInfo {
    pub pkgname: String,
    pub pkgbase: Option<String>,
    pub pkgver: String,
    pub pkgdesc: Option<String>,
    pub size: Option<i64>,
    pub url: Option<String>,
    pub arch: Option<String>,
    pub builddate: Option<String>,
    pub packager: Option<String>,
    pub groups: Vec<String>,
    pub licenses: Vec<String>,
    pub replaces: Vec<String>,
    pub conflicts: Vec<String>,
    pub provides: Vec<String>,
    pub depends: Vec<String>,
    pub optdepends: Vec<String>,
    pub makedepends: Vec<String>,
    pub checkdepends: Vec<String>,
}

impl PkgInfo {
    pub fn parse(source: &str) -> Result<PkgInfo, PkgError> {
        let mut pkginfo = PkgInfo {
            pkgname: "".into(),
            pkgbase: None,
            pkgver: "".into(),
            pkgdesc: None,
            size: None,
            url: None,
            arch: None,
            builddate: None,
            packager: None,
            groups: Vec::new(),
            licenses: Vec::new(),
            replaces: Vec::new(),
            conflicts: Vec::new(),
            provides: Vec::new(),
            depends: Vec::new(),
            optdepends: Vec::new(),
            makedepends: Vec::new(),
            checkdepends: Vec::new(),
        };
        for line in source
            .lines()
            .filter(|line| !line.starts_with('#'))
            .filter(|line| !line.is_empty())
        {
            let mut parts = line.splitn(2, '=');
            let key = parts.next().unwrap().trim();
            let value = match parts.next() {
                Some(value) => value.trim(),
                None => return Err(PkgError::MissingValue(line.into())),
            };
            match key {
                "pkgname" => pkginfo.pkgname = value.into(),
                "pkgbase" => pkginfo.pkgbase = Some(value.into()),
                "pkgver" => pkginfo.pkgver = value.into(),
                "pkgdesc" => pkginfo.pkgdesc = Some(value.into()),
                "size" => pkginfo.size = Some(value.parse()?),
                "url" => pkginfo.url = Some(value.into()),
                "arch" => pkginfo.arch = Some(value.into()),
                "builddate" => pkginfo.builddate = Some(value.into()),
                "packager" => pkginfo.packager = Some(value.into()),
                "group" => pkginfo.groups.push(value.into()),
                "license" => pkginfo.licenses.push(value.into()),
                "replace" => pkginfo.replaces.push(value.into()),
                "conflict" => pkginfo.conflicts.push(value.into()),
                "provide" => pkginfo.provides.push(value.into()),
                "depend" => pkginfo.depends.push(value.into()),
                "optdepend" => pkginfo.optdepends.push(value.into()),
                "makedepend" => pkginfo.makedepends.push(value.into()),
                "checkdepend" => pkginfo.checkdepends.push(value.into()),
                key => info!("skipping key in .PKGINFO: {}", key),
            }
        }
        if pkginfo.pkgname.is_empty() {
            Err(PkgError::MissingPkgName)
        } else if pkginfo.pkgver.is_empty() {
            Err(PkgError::MissingPkgVer)
        } else {
            Ok(pkginfo)
        }
    }
}

#[derive(Error, Debug)]
pub enum PkgError {
    #[error("missing value in '{0}'")]
    MissingValue(String),
    #[error("missing pkgname")]
    MissingPkgName,
    #[error("missing pkgver")]
    MissingPkgVer,
    #[error("missing .PKGINFO file in package")]
    MissingPkgInfo,
    #[error("invalid size")]
    InvalidSize(#[from] std::num::ParseIntError),
    #[error("failed reading package")]
    IoError(#[from] std::io::Error),
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::io::Cursor;
    use tar::{Archive, Builder, Header};
    use test_case::test_case;

    #[test]
    fn when_package_with_name_and_version_is_parsed_it_returns_that_name_and_version() {
        let archive = create_package("pkgname = some-package\npkgver = 0.1.2\n");
        let package = Package::from_archive(archive);
        let pkg_info = package.pkg_info().unwrap();

        assert_eq!(pkg_info.pkgname, "some-package");
        assert_eq!(pkg_info.pkgver, "0.1.2");
    }

    #[test]
    fn missing_items_in_pkginfo_are_not_included_in_the_result() {
        let archive = create_package("pkgname = some-package\npkgver = 0.1.2\n");
        let package = Package::from_archive(archive);
        let pkg_info = package.pkg_info().unwrap();

        assert_eq!(pkg_info.pkgbase, None);
        assert_eq!(pkg_info.pkgdesc, None);
        assert_eq!(pkg_info.size, None);
        assert_eq!(pkg_info.url, None);
        assert_eq!(pkg_info.arch, None);
        assert_eq!(pkg_info.builddate, None);
        assert_eq!(pkg_info.packager, None);
    }

    #[test]
    fn optional_fields_are_set_when_provided() {
        let archive = create_package(
            r#"pkgname = some-package
pkgver = 0.1.2
pkgbase = pkgbase
pkgdesc = pkgdesc
size = 1234
url = url
arch = arch
builddate = builddate
packager = packager"#,
        );
        let package = Package::from_archive(archive);
        let pkg_info = package.pkg_info().unwrap();

        assert_eq!(pkg_info.pkgbase, Some("pkgbase".into()));
        assert_eq!(pkg_info.pkgdesc, Some("pkgdesc".into()));
        assert_eq!(pkg_info.size, Some(1234));
        assert_eq!(pkg_info.url, Some("url".into()));
        assert_eq!(pkg_info.arch, Some("arch".into()));
        assert_eq!(pkg_info.builddate, Some("builddate".into()));
        assert_eq!(pkg_info.packager, Some("packager".into()));
    }

    #[test_case(vec![] ; "empty_vec")]
    #[test_case(vec!["foo".to_string()]; "single_item")]
    #[test_case(vec!["foo".to_string(), "bar".to_string()] ; "two_items")]
    fn optional_vector_fields_contain_all_defined_values(groups: Vec<String>) {
        let archive = create_package(&format!(
            r#"pkgname = some-package
pkgver = 0.1.2
{}"#,
            groups
                .iter()
                .map(|group| format!("group = {}", group))
                .collect::<Vec<String>>()
                .join("\n")
        ));
        let package = Package::from_archive(archive);
        let pkg_info = package.pkg_info().unwrap();

        assert_eq!(pkg_info.groups, groups);
    }

    #[test]
    fn missing_items_in_pkginfo_results_in_empty_vectors() {
        let archive = create_package("pkgname = some-package\npkgver = 0.1.2\n");
        let package = Package::from_archive(archive);
        let pkg_info = package.pkg_info().unwrap();

        assert!(pkg_info.groups.is_empty());
        assert!(pkg_info.licenses.is_empty());
        assert!(pkg_info.replaces.is_empty());
        assert!(pkg_info.conflicts.is_empty());
        assert!(pkg_info.provides.is_empty());
        assert!(pkg_info.depends.is_empty());
        assert!(pkg_info.optdepends.is_empty());
        assert!(pkg_info.makedepends.is_empty());
        assert!(pkg_info.checkdepends.is_empty());
    }

    #[test]
    fn missing_pkgver_should_return_and_error() {
        let archive = create_package("pkgname = some-package\n");
        let package = Package::from_archive(archive);
        let pkg_info = package.pkg_info();

        match pkg_info {
            Err(PkgError::MissingPkgVer) => (),
            r => panic!("expected Missing pkgver error got {:?}", r),
        }
    }

    #[test]
    fn missing_pkgname_should_return_and_error() {
        let archive = create_package("pkgver = 1.2.3\n");
        let package = Package::from_archive(archive);
        let pkg_info = package.pkg_info();

        match pkg_info {
            Err(PkgError::MissingPkgName) => (),
            r => panic!("expected Missing pkgname error got {:?}", r),
        }
    }

    fn create_package(contents: &str) -> Archive<impl Read> {
        let mut db = Builder::new(Vec::new());

        let mut header = Header::new_gnu();
        header.set_size(contents.len() as u64);
        header.set_cksum();
        header.set_mode(0o644);
        db.append_data(&mut header, ".PKGINFO", contents.as_bytes())
            .unwrap();

        Archive::new(Cursor::new(db.into_inner().unwrap()))
    }
}
