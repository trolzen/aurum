use crate::database::DatabaseError;

use super::*;
use s3mockserver::S3TestServer;

fn init() {
    let _ = env_logger::builder().is_test(false).try_init();
}

#[tokio::test]
async fn empty_database_returned_when_local_path_does_not_exist() {
    init();
    let tempdir = tempfile::tempdir().expect("could not create tempdir for database");
    let db_path = DbPath::Local(tempdir.path().join("does-not-exist.db.tar"));

    let database = db_path.fetch_db().await.expect("could not fetch database");
    assert_eq!(
        database
            .iter()
            .expect("could not iterate on database")
            .count(),
        0
    );
}

#[tokio::test]
async fn empty_database_returned_when_local_path_is_an_empty_file() {
    init();
    let test_db = tempfile::Builder::new()
        .suffix(".db.tar")
        .tempfile()
        .unwrap();
    let db_path = DbPath::Local(test_db.path().into());

    let database = db_path.fetch_db().await.expect("could not fetch database");
    assert_eq!(
        database
            .iter()
            .expect("could not iterate on database")
            .count(),
        0
    );
}

#[tokio::test]
async fn empty_database_returned_when_s3_object_does_not_exist() {
    init();
    let ts = S3TestServer::new().await;

    let client = s3::Client::from_conf(ts.config());
    client
        .create_bucket()
        .bucket("bucket")
        .send()
        .await
        .unwrap();

    let db_path = DbPath::S3 {
        uri: Uri::from_static("s3://bucket/does_not_exist"),
        client,
    };

    let database = db_path.fetch_db().await.expect("could not fetch database");
    assert_eq!(
        database
            .iter()
            .expect("could not iterate on database")
            .count(),
        0
    );
}

#[tokio::test]
async fn empty_database_returned_when_s3_object_is_an_empty_file() {
    init();
    let ts = S3TestServer::new().await;

    let client = s3::Client::from_conf(ts.config());
    client
        .create_bucket()
        .bucket("bucket")
        .send()
        .await
        .unwrap();
    client
        .put_object()
        .bucket("bucket")
        .key("object.db.tar")
        .send()
        .await
        .expect("uploading object failed");

    let db_path = DbPath::S3 {
        uri: Uri::from_static("s3://bucket/object.db.tar"),
        client,
    };

    let database = db_path.fetch_db().await.expect("could not fetch database");
    assert_eq!(
        database
            .iter()
            .expect("could not iterate on database")
            .count(),
        0
    );
}

#[tokio::test]
async fn downloading_a_database_returns_same_database_that_was_uploaded() {
    init();
    let ts = S3TestServer::new().await;

    let client = s3::Client::from_conf(ts.config());
    client
        .create_bucket()
        .bucket("bucket")
        .send()
        .await
        .unwrap();

    let db_path = DbPath::S3 {
        uri: Uri::from_static("s3://bucket/object.db.tar"),
        client,
    };
    let mut database = Database::new().unwrap();
    database
        .add_package("tests/pkgs/minimal/minimal-1-1-any.pkg.tar", false)
        .unwrap();
    db_path
        .write_db(&database)
        .await
        .expect("could not fetch database");

    let returned_database = db_path.fetch_db().await.expect("could not fetch database");
    assert_eq!(
        database
            .iter()
            .unwrap()
            .collect::<std::result::Result<Vec<_>, DatabaseError>>()
            .unwrap(),
        returned_database
            .iter()
            .unwrap()
            .collect::<std::result::Result<Vec<_>, DatabaseError>>()
            .unwrap(),
    );
}

#[tokio::test]
async fn downloading_a_files_db_returns_same_database_that_was_uploaded() {
    init();
    let ts = S3TestServer::new().await;

    let client = s3::Client::from_conf(ts.config());
    client
        .create_bucket()
        .bucket("bucket")
        .send()
        .await
        .unwrap();

    let db_path = DbPath::S3 {
        uri: Uri::from_static("s3://bucket/object.db.tar"),
        client,
    };
    let mut database = Database::new().unwrap();
    database
        .add_package("tests/pkgs/minimal/minimal-1-1-any.pkg.tar", true)
        .unwrap();
    db_path
        .write_files(&database)
        .await
        .expect("could not fetch files database");

    let returned_database = db_path
        .fetch_files()
        .await
        .expect("could not fetch files database");
    assert_eq!(
        database
            .iter()
            .unwrap()
            .collect::<std::result::Result<Vec<_>, DatabaseError>>()
            .unwrap(),
        returned_database
            .iter()
            .unwrap()
            .collect::<std::result::Result<Vec<_>, DatabaseError>>()
            .unwrap(),
    );
}

#[tokio::test]
async fn writing_a_package_stores_packages_in_s3_at_same_location_as_db() {
    init();
    let ts = S3TestServer::new().await;

    let bucket = "bucket";
    let database = "object.db.tar";
    let prefix = "/prefix";
    let package_location = "tests/pkgs/minimal";
    let package = "minimal-1-1-any.pkg.tar";
    let package_path = format!("{}/{}", package_location, package);

    let client = s3::Client::from_conf(ts.config());
    client
        .create_bucket()
        .bucket("bucket")
        .send()
        .await
        .unwrap();

    let db_path = DbPath::S3 {
        uri: Uri::from_str(&format!("s3://{}{}/{}", bucket, prefix, database)).unwrap(),
        client: client.clone(),
    };
    db_path
        .write_package(&package_path)
        .await
        .expect("could not upload package");

    let object = client
        .get_object()
        .bucket(bucket)
        .key(format!("{}/{}", &prefix, package))
        .send()
        .await
        .unwrap();

    assert_eq!(
        object.body.collect().await.unwrap().into_bytes(),
        fs::read(&package_path).unwrap(),
    );
}

#[tokio::test]
async fn removing_a_package_removes_if_from_s3() {
    init();
    let ts = S3TestServer::new().await;

    let bucket = "bucket";
    let database = "object.db.tar";
    let prefix = "/prefix";
    let package_location = "tests/pkgs/minimal";
    let package = "minimal-1-1-any.pkg.tar";
    let package_path = format!("{}/{}", package_location, package);

    let client = s3::Client::from_conf(ts.config());
    client
        .create_bucket()
        .bucket("bucket")
        .send()
        .await
        .unwrap();

    let db_path = DbPath::S3 {
        uri: Uri::from_str(&format!("s3://{}{}/{}", bucket, prefix, database)).unwrap(),
        client: client.clone(),
    };
    db_path
        .write_package(&package_path)
        .await
        .expect("could not upload package");
    db_path
        .remove_package(&package)
        .await
        .expect("could not remove package");

    let result = client
        .get_object()
        .bucket(bucket)
        .key(format!("{}/{}", &prefix, package))
        .send()
        .await;

    result.expect_err("no error");
}
