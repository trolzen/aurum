pub type Result<T> = std::result::Result<T, AuroraError>;
use std::{io, path::PathBuf};
use thiserror::Error;

#[derive(Error, Debug)]
pub enum AuroraError {
    #[error("missing filename from s3 uri: {0}")]
    MissingFileFromS3Uri(PathBuf),
    #[error("invalid s3 uri: '{s3_uri}'")]
    InvalidS3Uri {
        s3_uri: String,
        #[source]
        src: http::uri::InvalidUri,
    },
    #[error("invalid AWS_ENDPOINT_URL environment variable: '{endpoint_url}'")]
    InvalidAwsEndpointUrl {
        endpoint_url: String,
        #[source]
        src: http::uri::InvalidUri,
    },
    #[error("{0}")]
    IoError(#[from] io::Error),
    #[error("{0}")]
    S3Error(#[from] s3::Error),
    #[error("{0}")]
    AwsSdkError(#[from] s3::SdkError<s3::error::GetObjectError>),
    #[error("{0}")]
    ReadBodyError(#[from] smithy_http::byte_stream::Error),
}

impl AuroraError {
    pub fn is_not_exists(&self) -> bool {
        use AuroraError::*;
        match self {
            AwsSdkError(s3::SdkError::ServiceError {
                err:
                    s3::error::GetObjectError {
                        kind: s3::error::GetObjectErrorKind::NoSuchKey(_),
                        ..
                    },
                ..
            }) => true,
            _ => false,
        }
    }
}
