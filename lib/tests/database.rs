use aurora::{database::PackageDesc, Database};
use glob::glob;
use std::collections::HashSet;
use std::ops::Sub;
use std::{
    collections::HashMap,
    io::{Cursor, Read},
};
use std::{
    fs::{canonicalize, File},
    path::PathBuf,
};
use std::{
    path::Path,
    process::{Command, Stdio},
};
use tar::Archive;
use tempfile::tempdir;

mod open_database_from {
    use super::*;
    use test_case::test_case;

    #[test_case("db.tar")]
    #[test_case("db.tar.zst")]
    #[test_case("db.tar.xz")]
    #[test_case("db.tar.gz")]
    #[test_case("db.tar.bz2")]
    fn empty_archive(ext: &str) {
        let dir = tempfile::TempDir::new().unwrap();
        let db_path = dir.path().join("test").with_extension(ext);
        std::fs::write(&db_path, "").unwrap();
        run_repo_add(&db_path, "tests/pkgs/minimal/minimal-1-1-any.pkg.tar");
        run_repo_remove(&db_path, "minimal");

        let database = Database::from_file(&db_path).unwrap();

        assert!(
            database.try_is_empty().unwrap(),
            "database had files inside it: {:?}",
            database
        );
    }

    #[test_case("db.tar")]
    #[test_case("db.tar.zst")]
    #[test_case("db.tar.xz")]
    #[test_case("db.tar.gz")]
    #[test_case("db.tar.bz2")]
    fn archive_with_single_package(ext: &str) {
        let dir = tempfile::TempDir::new().unwrap();
        let db_path = dir.path().join("test").with_extension(ext);
        std::fs::write(&db_path, "").unwrap();
        run_repo_add(&db_path, "tests/pkgs/minimal/minimal-1-1-any.pkg.tar");

        let database = Database::from_file(&db_path).unwrap();

        assert_eq!(database.try_len().unwrap(), 1);
    }

    #[test_case("db.tar")]
    #[test_case("db.tar.zst")]
    #[test_case("db.tar.xz")]
    #[test_case("db.tar.gz")]
    #[test_case("db.tar.bz2")]
    fn archive_with_multiple_packages(ext: &str) {
        let dir = tempfile::TempDir::new().unwrap();
        let db_path = dir.path().join("test").with_extension(ext);
        std::fs::write(&db_path, "").unwrap();
        run_repo_add(&db_path, "tests/pkgs/minimal/minimal-1-1-any.pkg.tar");
        run_repo_add(&db_path, "tests/pkgs/test-pkg/test-pkg-1-1-any.pkg.tar");
        run_repo_add(
            &db_path,
            "tests/pkgs/multiple-licenes/multiple-licences-1-1-any.pkg.tar",
        );

        let database = Database::from_file(&db_path).unwrap();

        assert_eq!(database.try_len().unwrap(), 3);
    }

    #[test]
    fn file_with_no_extension() {
        let dir = tempfile::TempDir::new().unwrap();
        let db_path = dir.path().join("test");
        std::fs::write(&db_path, "").unwrap();

        let result = Database::from_file(&db_path).err().unwrap();

        assert_eq!(result.kind(), std::io::ErrorKind::InvalidInput);
    }

    #[test]
    fn file_with_unknown_extension() {
        let dir = tempfile::TempDir::new().unwrap();
        let db_path = dir.path().join("test.something");
        std::fs::write(&db_path, "").unwrap();

        let result = Database::from_file(&db_path).err().unwrap();

        assert_eq!(result.kind(), std::io::ErrorKind::InvalidInput);
    }
}

#[test]
#[ignore]
fn every_package_added_to_the_database_can_be_listed() {
    let mut database = Database::new().unwrap();
    let test_packages: Vec<_> = iter_test_packages("tests/pkgs/*/*.pkg.tar*").collect();
    for (test_package, _, _) in test_packages.iter() {
        database.add_package(&test_package, false).unwrap();
    }

    let db_packages = database
        .iter()
        .unwrap()
        .collect::<Result<Vec<_>, _>>()
        .unwrap();

    for (_, pkgname, pkgver) in test_packages.iter() {
        assert_contains_package(&db_packages, pkgname, pkgver);
    }
}

mod add_repo_and_database_both {
    use super::*;

    #[test]
    fn create_same_md5sum_for_a_package() {
        for (test_package, pkgname, pkgver) in iter_test_packages("tests/pkgs/*/*.pkg.tar*") {
            let mut database = Database::new().unwrap();

            database.add_package(&test_package, false).unwrap();

            let database_package_contents =
                get_database_contents(database, &to_desc_path(&pkgname, &pkgver));
            let repo_add_package_contents =
                gen_repo_add_pkg_file(&test_package, &to_desc_path(&pkgname, &pkgver));

            assert_eq!(
                get_value("%MD5SUM%", &repo_add_package_contents),
                get_value("%MD5SUM%", &database_package_contents)
            );
        }
    }

    #[test]
    fn create_same_sha256_for_a_package() {
        for (test_package, pkgname, pkgver) in iter_test_packages("tests/pkgs/*/*.pkg.tar*") {
            let mut database = Database::new().unwrap();

            database.add_package(&test_package, false).unwrap();

            let database_package_contents =
                get_database_contents(database, &to_desc_path(&pkgname, &pkgver));
            let repo_add_package_contents =
                gen_repo_add_pkg_file(&test_package, &to_desc_path(&pkgname, &pkgver));

            assert_eq!(
                get_value("%SHA256SUM%", &repo_add_package_contents),
                get_value("%SHA256SUM%", &database_package_contents)
            );
        }
    }

    #[test]
    fn have_the_same_csize() {
        for (test_package, pkgname, pkgver) in iter_test_packages("tests/pkgs/*/*.pkg.tar*") {
            let mut database = Database::new().unwrap();

            database.add_package(&test_package, false).unwrap();

            let database_package_contents =
                get_database_contents(database, &to_desc_path(&pkgname, &pkgver));
            let repo_add_package_contents =
                gen_repo_add_pkg_file(&test_package, &to_desc_path(&pkgname, &pkgver));

            assert_eq!(
                get_value("%CSIZE%", &repo_add_package_contents),
                get_value("%CSIZE%", &database_package_contents)
            );
        }
    }

    #[test]
    fn have_the_same_keys() {
        for (test_package, pkgname, pkgver) in iter_test_packages("tests/pkgs/*/*.pkg.tar*") {
            let mut database = Database::new().unwrap();

            database.add_package(&test_package, false).unwrap();

            let database_package_keys =
                get_database_contents(database, &to_desc_path(&pkgname, &pkgver))
                    .lines()
                    .filter(|line| line.starts_with("%"))
                    .map(str::to_string)
                    .collect::<HashSet<_>>();
            let repo_add_package_keys =
                gen_repo_add_pkg_file(&test_package, &to_desc_path(&pkgname, &pkgver))
                    .lines()
                    .filter(|line| line.starts_with("%"))
                    .map(str::to_string)
                    .collect::<HashSet<_>>();

            let keys_missing_from_repo_add = database_package_keys.sub(&repo_add_package_keys);
            let keys_missing_from_database = repo_add_package_keys.sub(&database_package_keys);
            assert!(
                keys_missing_from_repo_add.is_empty(),
                "found keys in database that are missing from repo_add: {:?}",
                keys_missing_from_repo_add
            );
            assert!(
                keys_missing_from_database.is_empty(),
                "found keys in repo_add that are missing from database: {:?}",
                keys_missing_from_database
            );
        }
    }
}

mod remove_package_that {
    use super::*;

    #[test]
    fn exists_inside_the_repo() {
        for (test_package, pkgname, pkgver) in iter_test_packages("tests/pkgs/*/*.pkg.tar*") {
            let mut database = Database::new().unwrap();

            database.add_package(&test_package, false).unwrap();
            database.remove_package(&pkgname, &pkgver).unwrap();

            let mut buf: Vec<u8> = Vec::new();
            database.write(&mut buf).unwrap();

            assert_file_not_in_archive(Cursor::new(buf), &to_desc_path(&pkgname, &pkgver))
        }
    }

    #[test]
    fn does_not_exist_inside_the_repo() {
        for (_, pkgname, pkgver) in iter_test_packages("tests/pkgs/*/*.pkg.tar*") {
            let mut database = Database::new().unwrap();

            assert!(match database.remove_package(&pkgname, &pkgver) {
                Err(err) if err.kind() == std::io::ErrorKind::NotFound => true,
                _ => false,
            });
        }
    }
}

fn iter_test_packages(pattern: &str) -> impl Iterator<Item = (PathBuf, String, String)> {
    glob(pattern).unwrap().map(|path| {
        let path = path.expect("failed to scan test packages");
        let pkgname = &path
            .file_name()
            .unwrap()
            .to_str()
            .unwrap()
            .split_terminator(".")
            .next()
            .unwrap();
        let pkgname = pkgname.strip_suffix("-any").unwrap_or(pkgname);
        let pkgname = pkgname.strip_suffix("-x86").unwrap_or(pkgname);
        let pkgname = pkgname.strip_suffix("-x86_64").unwrap_or(pkgname);
        let (version_pos, _) = pkgname
            .char_indices()
            .rev()
            .filter(|&(_, c)| c == '-')
            .nth(1)
            .unwrap();
        let (pkgname, pkgver) = pkgname.split_at(version_pos);
        (
            canonicalize(&path).unwrap(),
            pkgname.to_string(),
            pkgver[1..].to_string(),
        )
    })
}

fn get_database_contents(database: Database, file_in_archive: impl AsRef<Path>) -> String {
    let mut buf: Vec<u8> = Vec::new();
    database.write(&mut buf).unwrap();
    get_file_from_archive(Cursor::new(buf), file_in_archive)
}

fn gen_repo_add_pkg_file(
    test_package: impl AsRef<Path>,
    file_in_archive: impl AsRef<Path>,
) -> String {
    let dir = tempdir().unwrap();
    let db_file = dir.path().join("test.db.tar");

    run_repo_add(&db_file, test_package);

    get_file_from_archive(
        File::open(dir.path().join(&db_file)).unwrap(),
        file_in_archive,
    )
}

fn run_repo_add(db_path: impl AsRef<Path>, test_package: impl AsRef<Path>) {
    let status = Command::new("repo-add")
        .arg(db_path.as_ref())
        .arg(test_package.as_ref())
        .stdout(Stdio::null())
        .status()
        .expect("failed to execute process");
    assert!(status.success());
}

fn run_repo_remove(db_path: impl AsRef<Path>, test_package: &str) {
    let status = Command::new("repo-remove")
        .arg(db_path.as_ref())
        .arg(test_package)
        .stdout(Stdio::null())
        .status()
        .expect("failed to execute process");
    assert!(status.success());
}

fn get_file_from_archive(archive_file: impl Read, file_in_archive: impl AsRef<Path>) -> String {
    let file_in_archive = file_in_archive.as_ref();
    let archive = read_archive(archive_file);
    archive
        .get(file_in_archive)
        .expect(&format!(
            "failed to find '{}' in archive, it contains the files {:?}",
            file_in_archive.display(),
            archive.keys(),
        ))
        .to_string()
}

fn assert_file_not_in_archive(archive_file: impl Read, file_in_archive: impl AsRef<Path>) {
    let archive = read_archive(archive_file);
    assert_eq!(archive.get(file_in_archive.as_ref()), None);
}

fn assert_contains_package(db_packages: &[PackageDesc], pkgname: &str, pkgver: &str) {
    assert!(
        db_packages
            .iter()
            .find(|pkg_info| &pkg_info.name == pkgname && &pkg_info.version == pkgver)
            .is_some(),
        "{}-{} was not found in packages: {:?}",
        pkgname,
        pkgver,
        db_packages
    );
}

fn get_value<'a>(key: &str, content: &'a str) -> &'a str {
    let mut lines = content.lines();
    loop {
        match lines.next() {
            Some(line) if line == key => return lines.next().unwrap(),
            Some(_) => (),
            None => panic!("key not found"),
        };
    }
}

fn read_archive(reader: impl Read) -> HashMap<PathBuf, String> {
    let mut archive = Archive::new(reader);
    archive
        .entries()
        .unwrap()
        .map(|entry| entry.unwrap())
        .map(|mut entry| {
            let path = entry.path().unwrap().to_path_buf();
            let mut content = String::new();
            entry.read_to_string(&mut content).unwrap();
            (path, content)
        })
        .collect()
}

fn to_desc_path(pkgname: &str, pkgver: &str) -> String {
    format!("{}-{}/desc", pkgname, pkgver)
}
