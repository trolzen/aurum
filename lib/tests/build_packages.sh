#!/bin/bash
set -uo pipefail
trap 's=$?; echo "$0: Error on line "$LINENO": $BASH_COMMAND"; exit $s' ERR
IFS=$'\n\t'

for pkgdir in pkgs/*; do
    echo "Building $pkgdir"
    cd "$pkgdir"
    rm -f *.pkg.tar*
    PKGEXT='.pkg.tar' makepkg
    PKGEXT='.pkg.tar.zst' makepkg
    PKGEXT='.pkg.tar.xz' makepkg
    PKGEXT='.pkg.tar.gz' makepkg
    PKGEXT='.pkg.tar.bz2' makepkg
    cd -
done
